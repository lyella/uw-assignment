import forEachPolyfill from './components/forEachPolyfill';
import navigationToggle from './components/navigationToggle';
import slider from './components/slider';

document.addEventListener(
  'DOMContentLoaded',
  () => {
    forEachPolyfill()
    navigationToggle()
    slider()
  },
  false
)
