/**
* Slider using Flickity
*/

const slider = () => {
  const slider = document.querySelector('.js-slider');

  //@ts-ignore
  const flkty2 = new Flickity( slider, {
    cellAlign: 'left',
    contain: true,
    watchCSS: true
  });
}

export default slider
