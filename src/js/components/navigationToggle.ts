/**
* Navigation Toggle Button
*/

const navigationToggle = () => {
  const hamburger = document.querySelector(".js-mobile-header");
  const nav = document.querySelector(".js-nav");
  const navMenu = document.querySelector(".js-nav-menu");

  hamburger?.addEventListener ('click', () => {
    if (hamburger) {
      hamburger?.classList.toggle('active');
      navMenu?.classList.toggle('active');
      nav?.classList.toggle('active');
      document.body.classList.toggle('overflow'); 
    }
  });

  //make header fixed in position when scrolled
  const header = document.querySelector(".js-header");
  if(header) {
    window.addEventListener('scroll', () => {
      if(window.pageYOffset >= 100) {
        header?.classList.add('header--fixed');
      } else {
        header?.classList.remove('header--fixed');
      }
    })
  }
} 

export default navigationToggle